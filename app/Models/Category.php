<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $table = 'categories',
              $fillable = ['name', 'slug', 'description', 'image'];

    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'category_products');
    }

    public function deals()
    {
        return $this->belongsToMany(\App\Models\Deal::class, 'category_products');
    }
}
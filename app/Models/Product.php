<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    protected $table = 'products',
              $fillable = ['name', 'slug', 'description', 'image', 'price'];

    public function getPriceAttribute($value)
    {
    	return number_format($value, 2);
    }
}

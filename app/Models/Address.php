<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {
    protected $table = 'addresses',
              $fillable = ['user_id', 'line1', 'line2', 'town', 'postcode'];
}
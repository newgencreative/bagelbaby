<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {
    protected $table = 'settings',
              $fillable = ['key', 'value'],
              $appends = ['actual_value'];

    public static function get($key)
    {
    	$value = self::where('key', $key)->first();
    	return $value;
    }

    public function getActualValueAttribute()
    {
    	return json_decode($this->value, true);
    }
}
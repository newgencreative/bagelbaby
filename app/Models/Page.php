<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {
    protected $table = 'pages',
              $fillable = ['template_id', 'name', 'slug', 'content'];

    public function template()
    {
    	return $this->hasOne(\App\Models\Template::class, 'id', 'template_id');
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFilling extends Model {
    protected $table = 'product_fillings',
              $fillable = ['name'];
}
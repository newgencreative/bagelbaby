<?php

namespace App\Models;
use App\Models\ProductFilling;
use App\Models\ProductSauce;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model {
    protected $table = 'cart_items',
              $fillable = ['cart_id', 'product_id', 'fillings', 'sauces', 'cheese', 'quantity'],
              $appends = ['actual_fillings', 'actual_sauces', 'filling_list', 'sauce_list', 'price'];

    public function getPriceAttribute()
    {
        $price = 0;

        if($this->product) $price = number_format(($this->product->price * $this->quantity), 2);
        if($this->deal) $price = number_format(($this->deal->price * $this->quantity), 2);
        
        return $price;
    }

    public function getDealOptionsAttribute($value)
    {
        if(!$value) return null;

        $options = json_decode($value);

        foreach($options as &$option) {
            $fillings = [];
            $sauces = [];

            if($option->fillings) {
                foreach($option->fillings as $filling) {
                    $fillings[] = ProductFilling::find($filling);
                }
            }

            if($option->sauces) {
                foreach($option->sauces as $sauce) {
                    $sauces[] = ProductSauce::find($sauce);
                }
            }

            $option->fillings = $fillings;
            $option->sauces = $sauces;
        }

        return $options;
    }

    public function getActualFillingsAttribute()
    {
        if(!$this->fillings) return null;

    	$end_fillings = [];
    	$fillings = json_decode($this->fillings);

    	foreach($fillings as $filling) {
    		$end_fillings[] = ProductFilling::find($filling);
        }

    	return $end_fillings;
    }

    public function getActualSaucesAttribute()
    {
        if(!$this->sauces) return null;

    	$end_sauces = [];
    	$sauces = json_decode($this->sauces);

    	foreach($sauces as $sauce) {
    		$end_sauces[] = ProductSauce::find($sauce);
    	}

    	return $end_sauces;
    }

    public function getFillingListAttribute()
    {
        if(!$this->actual_fillings) return null;

        $list = '';
        foreach($this->actual_fillings as $filling) {
            $list .= $filling->name . ', ';
        }
        
        return substr($list, 0, -2);
    }

    public function getSauceListAttribute()
    {
        if(!$this->actual_sauces) return null;

        $list = '';
        foreach($this->actual_sauces as $sauce) {
            $list .= $sauce->name . ', ';
        }
        
        return substr($list, 0, -2);
    }

    public function product()
    {
        return $this->hasOne(\App\Models\Product::class, 'id', 'product_id');
    }

    public function deal()
    {
        return $this->hasOne(\App\Models\Deal::class, 'id', 'deal_id');
    }
}
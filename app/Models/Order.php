<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model {
    protected $table = 'orders',
              $fillable = ['user_id', 'guest_id', 'address_id', 'stripe_payment_id', 'status', 'total_price', 'pretty_id'],
              $appends = ['pretty_date', 'human_time', 'pretty_delivery_time'];

    public function getTotalPriceAttribute($value)
    {
        return number_format($value, 2);
    }

    public function getPrettyDateAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    public function getHumanTimeAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getPrettyDeliveryTimeAttribute()
    {
        $time = Carbon::parse($this->delivery_time);
        return $time->format('h:ia');
    }

    public function lines()
    {
    	return $this->hasMany(\App\Models\OrderLine::class, 'order_id', 'id');
    }

    public function user()
    {
    	return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

    public function address()
    {
        return $this->hasOne(\App\Models\Address::class, 'id', 'address_id');
    }

    public function guest()
    {
        return $this->hasOne(\App\Models\Guest::class, 'id', 'guest_id');
    }
}
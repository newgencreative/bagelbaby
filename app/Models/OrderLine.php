<?php

namespace App\Models;

use App\Models\ProductFilling;
use App\Models\ProductSauce;
use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model {
    protected $table = 'order_lines',
              $fillable = ['order_id', 'product_id', 'fillings', 'sauces'];

    public function getDealOptionsAttribute($value)
    {
        if(!$value) return null;

        return json_decode($value);
    }

    public function product()
    {
    	return $this->hasOne(\App\Models\Product::class, 'id', 'product_id');
    }

    public function deal()
    {
    	return $this->hasOne(\App\Models\Deal::class, 'id', 'deal_id');
    }
}
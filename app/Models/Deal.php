<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Deal extends Model {
    protected $table = 'deals',
              $fillable = ['name', 'slug', 'description', 'image', 'price'];

    public function getPriceAttribute($value)
    {
    	return number_format($value, 2);
    }

    public function getDealsAttribute($value)
    {
    	$deal_categories = json_decode($value);
    	$end_categories = [];

    	foreach($deal_categories as $category) {
            $cat = Category::with('products')->find($category);
    		$end_categories[$cat->display_order] = $cat;
    	}

    	return $end_categories;
    }
}

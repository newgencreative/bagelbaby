<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSauce extends Model {
    protected $table = 'product_sauces',
              $fillable = ['name'];
}
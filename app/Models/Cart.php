<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model {
    protected $table = 'cart',
              $fillable = ['cart_key', 'user_id'],
              $appends = ['total_items', 'total_price'];

    public static function getOrCreate()
    {
    	// If there's no cart key saved in session create one.
    	if(!session()->has('cart_key')) {
    		session()->put('cart_key', md5(time()));
    	}

    	// Use the cart key to get or create the cart model.
    	$cart_key = session()->get('cart_key');

    	$cart = Cart::where('cart_key', $cart_key)->with('items.product', 'items.deal')->first();
    	if(!$cart) {
    		$cart = Cart::create(['cart_key' => $cart_key]);
    	}

    	// Return the cart.
    	return $cart;
    }

    public function getTotalItemsAttribute()
    {
        return count($this->items);
    }

    public function getTotalPriceAttribute()
    {
        $price = 0;

        foreach($this->items as $item) {
            $price += $item->price;
        }

        // Add delivery
        if($price < 15) $price += 2.5;

        return number_format($price, 2);
    }

    public function removeItems()
    {
        foreach($this->items as $item) {
            $item->delete();
        }
    }

    public function items()
    {
        return $this->hasMany(\App\Models\CartItem::class, 'cart_id', 'id');
    }
}
<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductController extends Controller {
	public function single($slug)
	{
		$product = Product::where('slug', $slug)->first();

		return view('product', [
			'product' => $product,
		]);
	}
}
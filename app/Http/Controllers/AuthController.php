<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {

	public function login(Request $request)
	{
		$credentials = $request->only('email', 'password');

		if(Auth::attempt($credentials)) {
			return redirect()->to('/admin/products');
		}

		return redirect()->to('/login?error=invalid_credentials');
	}

	public function logout()
	{
		Auth::logout();

		return redirect()->to('/');
	}
	
}
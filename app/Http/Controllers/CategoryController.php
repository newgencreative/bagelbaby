<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller {
	public function all($slug = 'deals')
	{
		$categories = Category::with('products')
			->orderBy('display_order', 'ASC')
			->get();

		$category = Category::with('products')
			->where('slug', $slug)
			->first();

		return view('categories', ['categories' => $categories, 'category' => $category]);
	}

	public function single($slug)
	{
		$category = Category::where('slug', $slug)
			->with('products')
			->first();

		return view('category', ['category' => $category]);
	}
}
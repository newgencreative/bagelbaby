<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Guest;
use App\Models\Order;
use App\Models\OrderLine;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class CartController extends Controller {
	public function my(Request $request)
	{
		return view('cart');
	}

	public function add(Request $request)
	{
		// Find the cart.
		$cart = Cart::getOrCreate($request);

		// Create the item.
		$item = new CartItem;
		$item->cart_id = $cart->id;
		$item->product_id = $request->product_id;
		$item->cheese = ($request->cheese) ? $request->cheese : 1;
		$item->quantity = ($request->quantity) ? $request->quantity : 1;

		if($request->has('fillings'))
			$item->fillings = json_encode($request->fillings);

		if($request->has('sauces'))
			$item->sauces = json_encode($request->sauces);

		$item->save();

		// Return the cart.
		return Cart::getOrCreate($request);
	}

	public function edit($item, Request $request)
	{
		$item = CartItem::find($item);

		$item->cheese = ($request->cheese) ? $request->cheese : 1;
		$item->quantity = ($request->quantity) ? $request->quantity : 1;

		if($request->has('fillings'))
			$item->fillings = json_encode($request->fillings);

		if($request->has('sauces'))
			$item->sauces = json_encode($request->sauces);

		$item->save();

		// Return the cart.
		return Cart::getOrCreate($request);
	}

	public function addDeal(Request $request)
	{
		// Find the cart
		$cart = Cart::getOrCreate($request);

		foreach($request->deals as $deal) {
			$item = new CartItem;
			$item->cart_id = $cart->id;
			$item->deal_id = $request->deal_id;
			$item->deal_options = json_encode($deal);
			$item->save();
		}

		$item->save();

		// Return the cart
		return Cart::getOrCreate($request);
	}

	public function remove($item)
	{
		$item = CartItem::find($item);
		$item->delete();

		return redirect()->action('CartController@my');
	}

	public function apiRemove($item, Request $request)
	{
		$item = CartItem::find($item);
		$item->delete();

		return Cart::getOrCreate($request);
	}

	public function userCheckout(Request $request)
	{
		$cart = Cart::getOrCreate($request);

		// 404 if empty cart
		if($cart->total_items == 0) return app()->abort(404);

		// Make the payment intent
		$payment_intent = \Stripe\PaymentIntent::create([
		  	'amount' => ($cart->total_price * 100),
		  	'currency' => 'gbp',
		  	'payment_method_types' => ['card'],
		  	'customer' => auth()->user()->stripe_key,
		]);

		// Confirm the intent
		$payment_intent->confirm([
		  	'payment_method' => $request->card_id,
		]);

		// Create the order
		$order = new Order;
		$order->pretty_id = "" . Uuid::generate() . "";
		$order->user_id = auth()->id();
		$order->address_id = $request->address_id;
		$order->stripe_payment_id = $payment_intent->id;
		$order->status = 1;
		$order->total_price = $cart->total_price;
		$order->save();

		// Create the order lines
		foreach($cart->items as $item) {
			$line = new OrderLine;
			$line->order_id = $order->id;
			$line->product_id = $item->product_id;
			$line->fillings = $item->fillings;
			$line->sauces = $item->sauces;
			$line->cheese = $item->cheese;
			$line->save();
		}

		// Remove the cart items
		$cart->removeItems();

		// Return the order and lines
		return $order->load('lines');
	}

	public function guestCheckout(Request $request)
	{
		$cart = Cart::getOrCreate($request);

		// 404 if empty cart
		if($cart->total_items == 0) return app()->abort(404);

		// Make the payment intent
		$payment_intent = \Stripe\PaymentIntent::create([
		  	'amount' => ($cart->total_price * 100),
		  	'currency' => 'gbp',
		  	'payment_method' => $request->token,
		    'confirmation_method' => 'manual',
		    'confirm' => true,
		]);

		// Create the address
		$address = new Address;
		$address->line1 = $request->address_line_1;
		$address->town = $request->address_town;
		$address->postcode = $request->address_postcode;
		$address->save();

		// Create the guest
		$guest = new Guest;
		$guest->name = $request->name;
		$guest->email = $request->email;
		$guest->phone = $request->phone;
		$guest->save();

		// Create the order
		$order = new Order;
		$order->pretty_id = "" . Uuid::generate() . "";
		$order->guest_id = $guest->id;
		$order->address_id = $address->id;
		$order->stripe_payment_id = $payment_intent->id;
		$order->status = 1;
		$order->total_price = $cart->total_price;
		$order->save();

		// Create the order lines
		foreach($cart->items as $item) {
			$line = new OrderLine;
			$line->order_id = $order->id;

			if($item->product_id) {
				$line->product_id = $item->product_id;
				$line->fillings = $item->fillings;
				$line->sauces = $item->sauces;
				$line->cheese = $item->cheese;
			} else if($item->deal_id) {
				$line->deal_id = $item->deal_id;
				$line->deal_options = json_encode($item->deal_options);
			}

			$line->save();
		}

		// Remove the cart items
		$cart->removeItems();

		// Return the order and lines
		return $order->load('lines');
	}

	public function delivery(Request $request)
	{
		// Variables to hold dates.
		$opening_times = Setting::get('opening_times');
		$now = Carbon::now();
		$todays_day = strtolower($now->format('l'));
		$todays_opening_times = $opening_times->actual_value[$todays_day];

		// Opening/closing times
		$opening = Carbon::parse($now->format('Y-m-d') . ' ' . $todays_opening_times['opens'] . ':00');
		$closing = Carbon::parse($now->format('Y-m-d') . ' ' . $todays_opening_times['closes'] . ':00');

		$start = $opening->format('Hi');
		return $start;

		return [$now->format('Hi'), $opening->format('Hi')];

		return $closing->addMinutes(60)->format('H:i');

		return $now->format('H:i');
	}
}
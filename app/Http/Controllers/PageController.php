<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Deal;
use App\Models\Location;
use App\Models\Page;
use App\Models\Order;

class PageController extends Controller {
	public function home()
	{
		$categories = Category::with('products')
			->orderBy('display_order', 'ASC')
			->get();

		return view('home', ['fixed_header' => true, 'categories' => $categories]);
	}

	public function newhome()
	{
		return view('newhome');
	}

	public function newestHome()
	{
		return view('homeredesign');
	}

	public function menu($active = false)
	{
		return view('menu', ['active' => $active]);
	}

	public function deal($slug)
	{
		$categories = Category::with('products', 'deals')
			->orderBy('display_order', 'ASC')
			->get();

		$deal = Deal::where('slug', $slug)->firstOrFail();

		return view('deal', ['deal' => $deal, 'categories' => $categories]);
	}

	public function order($order_id)
	{
		$order = Order::where('pretty_id', $order_id)->with('lines.product', 'lines.deal', 'user', 'address', 'guest')->firstOrFail();

		$payment = \Stripe\PaymentIntent::retrieve($order->stripe_payment_id);

		return view('order', [
			'order' => $order,
			'payment' => $payment
		]);
	}

	public function about()
	{
		return view('about');
	}

	public function location()
	{
		return view('location');
	}

	public function calculateDistance()
	{
		
	}

	public function generic($slug = false)
	{
		$page = Page::where('slug', $slug)->with('template')->firstOrFail();
		return view($page->template->filename, ['page' => $page]);
	}
}
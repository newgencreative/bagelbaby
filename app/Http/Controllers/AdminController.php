<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductFilling;
use App\Models\ProductSauce;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AdminController extends Controller {

	public function dashboard()
	{
		return redirect()->to('/admin/products');
	}

	public function products(Request $request)
	{
		$products = Product::all();

		return view('admin.products.all', [
			'products' => $products,
			'saved' => ($request->has('saved')) ? true : false,
		]);
	}

	public function editProduct(Product $product, Request $request)
	{
		return view('admin.products.edit', [
			'product' => $product,
			'saved' => ($request->has('saved')) ? true : false,
		]);
	}

	public function doEdit(Product $product, Request $request)
	{
		$product->name = $request->name;
		$product->slug = Str::slug($request->slug);
		$product->tagline = $request->tagline;
		$product->price = $request->price;
		$product->description = $request->description;
		$product->long_description = $request->long_description;

		if($request->has('image')) {
			// Image
			$imageName = time().'.'.request()->image->getClientOriginalExtension();
	        request()->image->move(public_path('images/products'), $imageName);
			
			$product->image = '/images/products/' . $imageName;
			$product->image_thumbnail = '/images/products/' . $imageName;
		}

		$product->cheese_enabled = ($request->has('cheese')) ? 1 : 0;
		$product->sauces_enabled = ($request->has('sauces_enabled')) ? 1 : 0;
		$product->fillings_enabled = ($request->has('fillings_enabled')) ? 1 : 0;
		$product->save();

		return redirect()->to('/admin/products/' . $product->id . '?saved=true');
	}

	public function newProduct()
	{
		return view('admin.products.new');
	}

	public function doNew(Request $request)
	{
		// Product
		$product = new Product;
		$product->name = $request->name;
		$product->slug = Str::slug($request->slug);
		$product->tagline = $request->tagline;
		$product->price = $request->price;
		$product->description = $request->description;
		$product->long_description = $request->long_description;

		// Image
		$imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/products'), $imageName);

		$product->image = '/images/products/' . $imageName;
		$product->image_thumbnail = '/images/products/' . $imageName;

		$product->cheese_enabled = ($request->has('cheese')) ? 1 : 0;
		$product->sauces_enabled = ($request->has('sauces_enabled')) ? 1 : 0;
		$product->fillings_enabled = ($request->has('fillings_enabled')) ? 1 : 0;
		$product->save();

		return redirect()->to('/admin/products?saved=true');
	}

	public function login(Request $request)
	{
		return view('admin.login', [
			'error' => ($request->has('error')) ? $request->error : false,
		]);
	}

	public function categories()
	{
		$categories = Category::all();

		return view('admin.products.categories', ['categories' => $categories]);
	}

	public function newCategory()
	{
		return view('admin.products.newcategory');
	}

	public function doNewCategory(Request $request)
	{
		$current_do = Category::limit(1)->orderBy('display_order', 'DESC')->first();
		$order = $current_do->display_order + 1;

		$category = new Category;
		$category->name = $request->name;
		$category->slug = Str::slug($request->slug);
		$category->display_order = $order;
		$category->save();

		return redirect()->to('/admin/products/categories');
	}

	public function editCategory(Category $category, Request $request)
	{
		return view('admin.products.editcategory', [
			'category' => $category,
			'saved' => ($request->has('saved')) ? true : false,
		]);
	}

	public function doEditCategory(Category $category, Request $request)
	{
		$category->name = $request->name;
		$category->slug = $request->slug;
		$category->save();

		return redirect()->to('/admin/products/categories/' . $category->id . '?saved=true');
	}

	public function sauces()
	{
		$sauces = ProductSauce::all();

		return view('admin.products.sauces', ['sauces' => $sauces]);
	}

	public function newSauce()
	{
		return view('admin.products.newsauce');
	}

	public function doNewSauce(Request $request)
	{
		$sauce = new ProductSauce;
		$sauce->name = $request->name;
		$sauce->price = $request->price;
		$sauce->description = $request->description;

		$imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/products/sauces'), $imageName);

		$sauce->image = '/images/products/sauces/' . $imageName;
		$sauce->save();

		return redirect()->to('/admin/products/sauces');
	}

	public function fillings()
	{
		$fillings = ProductFilling::all();

		return view('admin.products.fillings', ['fillings' => $fillings]);
	}

	public function newFilling()
	{
		return view('admin.products.newfilling');
	}

	public function doNewFilling(Request $request)
	{
		$filling = new ProductFilling;
		$filling->name = $request->name;
		$filling->price = $request->price;
		$filling->description = $request->description;

		$imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/products/fillings'), $imageName);

		$filling->image = '/images/products/fillings/' . $imageName;
		$filling->save();

		return redirect()->to('/admin/products/fillings');
	}

	public function editFilling(ProductFilling $filling, Request $request)
	{
		return view('admin.products.editfilling', [
			'filling' => $filling,
			'saved' => ($request->has('saved')) ? true : false,
		]);
	}

	public function doEditFilling(ProductFilling $filling, Request $request)
	{
		$filling->name = $request->name;
		$filling->price = $request->price;
		$filling->description = $request->description;

		if($request->image) {
			$imageName = time().'.'.request()->image->getClientOriginalExtension();
	        request()->image->move(public_path('images/products/fillings'), $imageName);
			$filling->image = '/images/products/fillings/' . $imageName;
		}

		$filling->save();

		return redirect()->to('/admin/products/fillings/' . $filling->id . '?saved=true');
	}

	public function editSauce(ProductSauce $sauce, Request $request)
	{
		return view('admin.products.editsauce', [
			'sauce' => $sauce,
			'saved' => ($request->has('saved')) ? true : false,
		]);
	}

	public function doEditSauce(ProductSauce $sauce, Request $request)
	{
		$sauce->name = $request->name;
		$sauce->price = $request->price;
		$sauce->description = $request->description;

		if($request->image) {
			$imageName = time().'.'.request()->image->getClientOriginalExtension();
	        request()->image->move(public_path('images/products/sauces'), $imageName);
			$sauce->image = '/images/products/sauces/' . $imageName;
		}

		$sauce->save();

		return redirect()->to('/admin/products/sauces/' . $sauce->id . '?saved=true');
	}

	public function orders()
	{
		$orders = Order::with('guest')->orderBy('created_at', 'DESC')->get();

		return view('admin.orders.all', ['orders' => $orders]);
	}
}
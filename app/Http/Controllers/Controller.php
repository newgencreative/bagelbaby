<?php

namespace App\Http\Controllers;

use App\Models\ProductFilling;
use App\Models\ProductSauce;
use App\Models\Cart;
use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $categories = Category::with('products', 'deals')
            ->orderBy('display_order', 'ASC')
            ->get();

    	// Common variables for views
    	View::share('sauces', ProductSauce::all());
    	View::share('fillings', ProductFilling::all());
    	View::share('cart', Cart::getOrCreate());
        View::share('categories', $categories);
        if(auth()->user()) View::share('user', auth()->user()->load('addresses'));

    	// Stripe configuration
    	\Stripe\Stripe::setApiKey('sk_test_4U5IewVwYQfGhL3y7ojzCSoq001aCPnLjB');
    }
}

<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Address;
use Illuminate\Http\Request;

class AccountController extends Controller {

	public function home()
	{
		return view('account.home', ['user' => auth()->user()]);
	}

	public function orders()
	{
		$orders = Order::where('user_id', auth()->id())->with('lines')->get();
		
		return view('account.orders', ['orders' => $orders]);
	}

	public function order($order_id)
	{
		$order = Order::where('pretty_id', $order_id)->with('lines.product', 'user', 'address', 'guest')->firstOrFail();

		$payment = \Stripe\PaymentIntent::retrieve($order->stripe_payment_id);

		return view('account.order', [
			'order' => $order,
			'payment' => $payment
		]);
	}

	public function addresses(Request $request)
	{
		$addresses = Address::where('user_id', auth()->id())->get();
		return view('account.addresses', ['addresses' => $addresses]);
	}

	public function addressesAjax(Request $request)
	{
		$addresses = Address::where('user_id', auth()->id())->get();
		return $addresses;
	}

	public function payments()
	{
		$cards = \Stripe\PaymentMethod::all([
		  	'customer' => auth()->user()->stripe_key,
		  	'type' => 'card',
		]);

		return view('account.payments', ['cards' => $cards->data]);
	}

	public function cards(Request $request)
	{
		$cards = \Stripe\PaymentMethod::all([
		  	'customer' => auth()->user()->stripe_key,
		  	'type' => 'card',
		]);

		return $cards;
	}

	public function createCard(Request $request)
	{
		// Calculate expiry
		$expiry = explode('/', $request->expiry);

		// Create the card
		$card = \Stripe\PaymentMethod::create([
		  	'type' => 'card',
		  	'card' => [
		    	'number' => $request->card,
		    	'exp_month' => trim($expiry[0]),
		    	'exp_year' => trim($expiry[1]),
		    	'cvc' => $request->cvc,
		  	]
		]);

		// Attach the card to the customer
		$card->attach(['customer' => auth()->user()->stripe_key]);

		return $card;
	}

	public function createAddress(Request $request)
	{
		$address = new Address;
		$address->user_id = (auth()->user()) ? auth()->id() : null;
		$address->line1 = $request->line1;
		$address->line2 = $request->line2;
		$address->town = $request->town;
		$address->postcode = $request->postcode;
		$address->save();

		return $address;
	}

}
<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Egg & Bacon',
            'slug' => 'egg-and-bacon',
            'tagline' => "Eggs like you've never seen them",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.',
            'long_description' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam convallis neque at mauris blandit tempor. Aenean tincidunt bibendum orci, ut imperdiet neque vehicula eu. Vivamus iaculis interdum varius. Nam tincidunt felis in viverra tincidunt. Nullam facilisis, erat efficitur sollicitudin rutrum, leo lacus mollis ipsum, eu efficitur est quam in sapien.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam convallis neque at mauris blandit tempor. Aenean tincidunt bibendum orci, ut imperdiet neque vehicula eu. Vivamus iaculis interdum varius. Nam tincidunt felis in viverra tincidunt. Nullam facilisis, erat efficitur sollicitudin rutrum, leo lacus mollis ipsum, eu efficitur est quam in sapien.</p>",
            'image' => '/images/products/egg-and-bacon.jpg',
            'image_thumbnail' => '/images/products/egg-and-bacon-thumb.jpg',
            'price' => 10.00,
        ]);

        DB::table('products')->insert([
            'name' => 'Tuna & Sweetcorn',
            'slug' => 'tuna-and-sweetcorn',
            'tagline' => "Tuna like you've never seen it",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.',
            'long_description' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam convallis neque at mauris blandit tempor. Aenean tincidunt bibendum orci, ut imperdiet neque vehicula eu. Vivamus iaculis interdum varius. Nam tincidunt felis in viverra tincidunt. Nullam facilisis, erat efficitur sollicitudin rutrum, leo lacus mollis ipsum, eu efficitur est quam in sapien.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam convallis neque at mauris blandit tempor. Aenean tincidunt bibendum orci, ut imperdiet neque vehicula eu. Vivamus iaculis interdum varius. Nam tincidunt felis in viverra tincidunt. Nullam facilisis, erat efficitur sollicitudin rutrum, leo lacus mollis ipsum, eu efficitur est quam in sapien.</p>",
            'image' => '/images/products/tuna-and-sweetcorn.jpg',
            'image_thumbnail' => '/images/products/tuna-and-sweetcorn-thumb.jpg',
            'price' => 10.00,
        ]);

        DB::table('products')->insert([
            'name' => 'Jerk Chicken',
            'slug' => 'jerk-chicken',
            'tagline' => "Chicken like you've never seen it",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.',
            'long_description' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam convallis neque at mauris blandit tempor. Aenean tincidunt bibendum orci, ut imperdiet neque vehicula eu. Vivamus iaculis interdum varius. Nam tincidunt felis in viverra tincidunt. Nullam facilisis, erat efficitur sollicitudin rutrum, leo lacus mollis ipsum, eu efficitur est quam in sapien.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam convallis neque at mauris blandit tempor. Aenean tincidunt bibendum orci, ut imperdiet neque vehicula eu. Vivamus iaculis interdum varius. Nam tincidunt felis in viverra tincidunt. Nullam facilisis, erat efficitur sollicitudin rutrum, leo lacus mollis ipsum, eu efficitur est quam in sapien.</p>",
            'image' => '/images/products/jerk-chicken.jpg',
            'image_thumbnail' => '/images/products/jerk-chicken-thumb.jpg',
            'price' => 10.00,
        ]);
    }
}

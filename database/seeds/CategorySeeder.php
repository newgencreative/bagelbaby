<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Bagels',
            'slug' => 'bagels',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.',
            'image' => '/images/categories/bagels.jpg'
        ]);

        DB::table('categories')->insert([
            'name' => 'Drinks',
            'slug' => 'drinks',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.',
            'image' => '/images/categories/drinks.jpg'
        ]);

        DB::table('categories')->insert([
            'name' => 'Desserts',
            'slug' => 'desserts',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.',
            'image' => '/images/categories/desserts.jpg'
        ]);
    }
}

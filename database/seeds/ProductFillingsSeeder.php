<?php

use Illuminate\Database\Seeder;

class ProductFillingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_fillings')->insert([
            'name' => 'Lettuce',
        ]);

        DB::table('product_fillings')->insert([
            'name' => 'Cucumber',
        ]);

        DB::table('product_fillings')->insert([
            'name' => 'Tomato',
        ]);
    }
}

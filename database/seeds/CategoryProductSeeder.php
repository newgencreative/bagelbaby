<?php

use Illuminate\Database\Seeder;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_products')->insert([
            'category_id' => 1,
            'product_id' => 1,
        ]);

        DB::table('category_products')->insert([
            'category_id' => 1,
            'product_id' => 2,
        ]);

        DB::table('category_products')->insert([
            'category_id' => 1,
            'product_id' => 3,
        ]);
    }
}

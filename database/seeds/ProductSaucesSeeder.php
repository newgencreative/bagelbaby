<?php

use Illuminate\Database\Seeder;

class ProductSaucesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_sauces')->insert([
            'name' => 'Jerk',
        ]);

        DB::table('product_sauces')->insert([
            'name' => 'BBQ',
        ]);

        DB::table('product_sauces')->insert([
            'name' => 'Sweet Chilli',
        ]);

        DB::table('product_sauces')->insert([
            'name' => 'Ketchup',
        ]);

        DB::table('product_sauces')->insert([
            'name' => 'Mayonnaise',
        ]);

        DB::table('product_sauces')->insert([
            'name' => 'Burger Sauce',
        ]);
    }
}

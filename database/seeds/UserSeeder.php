<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Stripe\Stripe::setApiKey('sk_test_4U5IewVwYQfGhL3y7ojzCSoq001aCPnLjB');
        
        $customer = \Stripe\Customer::create([
            'description' => 'Customer for joey@newgencreative.co.uk',
            'email' => 'joey@newgencreative.co.uk',
        ]);

        DB::table('users')->insert([
            'name' => 'Joey Emery',
            'email' => 'joey@newgencreative.co.uk',
            'password' => Hash::make('password'),
            'stripe_key' => $customer->id,
        ]);
    }
}

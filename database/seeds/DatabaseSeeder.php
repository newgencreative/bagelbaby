<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(CategoryProductSeeder::class);
        $this->call(ProductFillingsSeeder::class);
        $this->call(ProductSaucesSeeder::class);
        $this->call(UserSeeder::class);
    }
}

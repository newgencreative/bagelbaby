require('./bootstrap');

// Popups
Vue.component('popup-add-to-cart', require('./components/popup-add-to-cart.vue').default);
Vue.component('popup-login', require('./components/popup-login.vue').default);
Vue.component('popup-register', require('./components/popup-register.vue').default);
Vue.component('popup-new-card', require('./components/popup-new-card.vue').default);
Vue.component('popup-new-address', require('./components/popup-new-address.vue').default);
Vue.component('modal-product', require('./components/modal-product.vue').default);
Vue.component('modal-product-info', require('./components/modal-product-info.vue').default);

// Buttons
Vue.component('button-add-to-cart', require('./components/button-add-to-cart.vue').default);
Vue.component('button-open-mobile-menu', require('./components/button-open-mobile-menu.vue').default);
Vue.component('button-close-mobile-menu', require('./components/button-close-mobile-menu.vue').default);

// Badges
Vue.component('badge-cart', require('./components/badge-cart.vue').default);

// Payments
Vue.component('payment-user', require('./components/payment-user.vue').default);
Vue.component('payment-guest', require('./components/payment-guest.vue').default);

// Lists
Vue.component('list-address', require('./components/list-address.vue').default);

// Inputs
Vue.component('input-location-search', require('./components/input-location-search.vue').default);
Vue.component('input-postcode', require('./components/input-postcode.vue').default);

// Cart
Vue.component('smart-cart', require('./components/smart-cart.vue').default);

// Menu
Vue.component('smart-menu', require('./components/smart-menu.vue').default);

// Deal
Vue.component('smart-deal', require('./components/smart-deal.vue').default);

// Mount Vue
const app = new Vue({
	el: '#app',

	data() {
		return {
			activeAddProduct: false,
			cart: {
				total_items: window._cart.total_items,
			}
		}
	},
});
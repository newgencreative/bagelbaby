@extends('generic.newlayout')

@section('header')
	@include('generic.newheader')
@endsection

@section('content')
	<div class="page page-deal">
		<smart-deal></smart-deal>
	</div>

	<modal-product action="deal"></modal-product>
	<smart-cart minimized="true"></smart-cart>
@endsection

@section('footer')
	@include('generic.footer')
@endsection

@section('scripts')
	<script type="text/javascript">
		var _categories = {!! $categories !!};
		var _deal = {!! $deal !!};
		var _sauces = {!! $sauces !!};
		var _fillings = {!! $fillings !!};
	</script>
@endsection
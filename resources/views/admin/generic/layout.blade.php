<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="admin">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BagelBaby Administration</title>
        <link href="/css/app.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        @include('admin.generic.header')
    	@yield('content')

        @yield('scripts')
    </body>
</html>
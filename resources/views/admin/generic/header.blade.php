<header class="admin">
	<h1>Administration</h1>
	<ul>
		<li><a href="/auth/logout">Logout</a></li>
		<li @if(Route::currentRouteName() == "admin_orders")class="active"@endif><a href="{{ route('admin_orders') }}">Orders</a></li>
		<li @if(Route::currentRouteName() == "admin_products" || Route::currentRouteName() == "admin_product" || Route::currentRouteName() == "admin_product_new")class="active"@endif><a href="{{ route('admin_products') }}">Products</a></li>
		<!-- <li @if(Route::currentRouteName() == "admin_dashboard")class="active"@endif><a href="{{ route('admin_dashboard') }}">Dashboard</a></li> -->
		<li class="float-left"><a href="{{ route('home') }}"><span class="fa fa-chevron-left"></span> Back to website</a></li>
	</ul>
</header>
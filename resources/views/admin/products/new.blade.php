@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')

	<div class="content">
		<div class="inner">
			<form action="?" method="post" class="admin" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="name" class="form-control" placeholder="Name" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="slug" class="form-control" placeholder="Slug" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="tagline" class="form-control" placeholder="Tagline" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="price" class="form-control" placeholder="Price (£0.00)" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea class="form-control" name="description" placeholder="Short description" rows="5"></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea class="form-control" name="long_description" placeholder="Long description" rows="5"></textarea>
						</div>
					</div>
					<div class="col-md-3">
						<input type="file" name="image" />
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="pretty p-svg p-curve">
						        <input type="checkbox" name="cheese" />
						        <div class="state p-success">
						            <svg class="svg svg-icon" viewBox="0 0 20 20"><path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path></svg>
						            <label>Cheese enabled</label>
						        </div>
						    </div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="pretty p-svg p-curve">
						        <input type="checkbox" name="sauces_enabled" />
						        <div class="state p-success">
						            <svg class="svg svg-icon" viewBox="0 0 20 20"><path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path></svg>
						            <label>Sauces enabled</label>
						        </div>
						    </div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="pretty p-svg p-curve">
						        <input type="checkbox" name="fillings_enabled" />
						        <div class="state p-success">
						            <svg class="svg svg-icon" viewBox="0 0 20 20"><path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path></svg>
						            <label>Salads enabled</label>
						        </div>
						    </div>
						</div>
					</div>
					<div class="col-md-12">
						<input type="submit" class="btn btn-primary" value="Create product" />
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')
	
	<div class="content">
		<div class="inner">
			<div class="box-list">
				<div class="row">
					@foreach($categories as $category)
						<div class="col-md-3">
							<div class="box-list-item">
								<h2>{{ $category->name }}</h2>
								<a href="{{ route('admin_product_categories_edit', ['category' => $category]) }}" class="btn btn-primary">Edit category</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
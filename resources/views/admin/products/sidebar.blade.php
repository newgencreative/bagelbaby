<div class="sidebar">
	<ul>
		<li @if(Route::currentRouteName() == "admin_products")class="active"@endif><a href="{{ route('admin_products') }}"><span class="fa fa-list-alt fa-fw"></span> View all products</a></li>
		<li @if(Route::currentRouteName() == "admin_product_new")class="active"@endif><a href="{{ route('admin_product_new') }}"><span class="fa fa-plus-square fa-fw"></span> Add new product</a></li>
		<li @if(Route::currentRouteName() == "admin_product_categories")class="active"@endif><a href="{{ route('admin_product_categories') }}"><span class="fa fa-list-alt fa-fw"></span> View all categories</a></li>
		<li @if(Route::currentRouteName() == "admin_product_categories_new")class="active"@endif><a href="{{ route('admin_product_categories_new') }}"><span class="fa fa-plus-square fa-fw"></span> Add new category</a></li>
		<li @if(Route::currentRouteName() == "admin_product_sauces")class="active"@endif><a href="{{ route('admin_product_sauces') }}"><span class="fa fa-list-alt fa-fw"></span> View all sauces</a></li>
		<li @if(Route::currentRouteName() == "admin_product_sauces_new")class="active"@endif><a href="{{ route('admin_product_sauces_new') }}"><span class="fa fa-plus-square fa-fw"></span> Add new sauce</a></li>
		<li @if(Route::currentRouteName() == "admin_product_fillings")class="active"@endif><a href="{{ route('admin_product_fillings') }}"><span class="fa fa-list-alt fa-fw"></span> View all fillings</a></li>
		<li @if(Route::currentRouteName() == "admin_product_fillings_new")class="active"@endif><a href="{{ route('admin_product_fillings_new') }}"><span class="fa fa-plus-square fa-fw"></span> Add new filling</a></li>
	</ul>
</div>
@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')
	
	<div class="content">
		<div class="inner">
			<div class="box-list">
				<div class="row">
					@foreach($fillings as $filling)
						<div class="col-md-3">
							<div class="box-list-item">
								<h2>{{ $filling->name }}</h2>
								<a href="{{ route('admin_product_fillings_edit', ['filling' => $filling]) }}" class="btn btn-primary">Edit filling</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
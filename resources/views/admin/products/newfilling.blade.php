@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')

	<div class="content">
		<div class="inner">
			<form action="?" method="post" class="admin" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="name" class="form-control" placeholder="Name" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="price" class="form-control" placeholder="Price" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="description" class="form-control" placeholder="Description" />
						</div>
					</div>
					<div class="col-md-3">
						<input type="file" name="image" />
					</div>
					<div class="col-md-12">
						<input type="submit" class="btn btn-primary" value="Create filling" />
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
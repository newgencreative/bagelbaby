@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')
	
	<div class="content">
		<div class="inner">
			<div class="box-list">
				<div class="row">
					@foreach($sauces as $sauce)
						<div class="col-md-3">
							<div class="box-list-item">
								<h2>{{ $sauce->name }}</h2>
								<a href="{{ route('admin_product_sauces_edit', ['sauce' => $sauce]) }}" class="btn btn-primary">Edit sauce</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
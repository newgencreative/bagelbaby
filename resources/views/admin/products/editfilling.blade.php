@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')

	<div class="content">
		<div class="inner">
			<form action="?" method="post" class="admin" enctype="multipart/form-data">
				@if($saved)
					<div class="alert alert-success"><p>Your changes were saved</p></div>
				@endif
				
				@csrf
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="name" class="form-control" placeholder="Name" value="{{ $filling->name }}" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="price" class="form-control" placeholder="Price" value="{{ $filling->price }}" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" name="description" class="form-control" placeholder="Description" value="{{ $filling->description }}" />
						</div>
					</div>
					<div class="col-md-3">
						<input type="file" name="image" />
					</div>
					<div class="col-md-12">
						<input type="submit" class="btn btn-primary" value="Save changes" />
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
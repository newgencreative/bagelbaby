@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')
	
	<div class="content">
		<div class="inner">
			<div class="box-list">
				<div class="row">
					@foreach($products as $product)
						<div class="col-md-3">
							<div class="box-list-item">
								<img src="{{ $product->image }}" />
								<h2>{{ $product->name }}</h2>
								<h3>&pound;{{ $product->price }}</h3>
								<a href="{{ route('admin_product', ['product' => $product]) }}" class="btn btn-primary">Edit product</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
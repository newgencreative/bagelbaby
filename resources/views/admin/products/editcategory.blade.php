@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')

	<div class="content">
		<div class="inner">
			<form action="?" method="post" class="admin">
				@if($saved)
					<div class="alert alert-success"><p>Your changes were saved</p></div>
				@endif
				
				@csrf
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" name="name" class="form-control" placeholder="Name" value="{{ $category->name }}" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" name="slug" class="form-control" placeholder="Slug" value="{{ $category->slug }}" />
						</div>
					</div>
					<div class="col-md-12">
						<input type="submit" class="btn btn-primary" value="Save category" />
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
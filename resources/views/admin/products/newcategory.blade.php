@extends('admin.generic.layout')

@section('content')
	@include('admin.products.sidebar')

	<div class="content">
		<div class="inner">
			<form action="?" method="post" class="admin">
				@csrf
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" name="name" class="form-control" placeholder="Name" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" name="slug" class="form-control" placeholder="Slug" />
						</div>
					</div>
					<div class="col-md-12">
						<input type="submit" class="btn btn-primary" value="Create category" />
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
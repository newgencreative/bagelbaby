<div class="sidebar">
	<ul>
		<li @if(Route::currentRouteName() == "admin_orders")class="active"@endif><a href="{{ route('admin_orders') }}"><span class="fa fa-list-alt fa-fw"></span> View all orders</a></li>
	</ul>
</div>
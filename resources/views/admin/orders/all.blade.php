@extends('admin.generic.layout')

@section('content')
	@include('admin.orders.sidebar')

	<div class="content">
		<div class="inner">
			<div class="box-list">
				<div class="row">
					@foreach($orders as $order)
						<div class="col-md-3">
							<div class="box-list-item">
								<h2>{{ $order->guest->name }}</h2>
								<h3>&pound;{{ $order->total_price }}</h3>
								<a href="/ordersnew/{{ $order->pretty_id }}" class="btn btn-primary" target="_blank">View order</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
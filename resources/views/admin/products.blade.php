@extends('admin.generic.layout')

@section('content')
	<div class="sidebar">
		<ul>
			<li @if(Route::currentRouteName() == "admin_products")class="active"@endif><a href="{{ route('admin_products') }}"><span class="fa fa-list-alt fa-fw"></span> View all products</a></li>
			<li @if(Route::currentRouteName() == "admin_product_new")class="active"@endif><a href="{{ route('admin_product_new') }}"><span class="fa fa-plus-square fa-fw"></span> Add new product</a></li>
			<li><a href="#"><span class="fa fa-search fa-fw"></span> Search for a product</a></li>
		</ul>
	</div>

	<div class="content">
		<div class="inner">
			<div class="box-list">
				<div class="row">
					@foreach($products as $product)
						<div class="col-md-3">
							<div class="box-list-item">
								<img src="{{ $product->image_thumbnail }}" />
								<h2>{{ $product->name }}</h2>
								<h3>&pound;{{ $product->price }}</h3>
								<a href="{{ route('admin_product', ['product' => $product]) }}" class="btn btn-primary">Edit product</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
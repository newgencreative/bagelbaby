@extends('admin.generic.blanklayout')

@section('content')
	<div class="login-page">
		<div class="container">
			<img class="logo" src="images/logo-condensed.png" />
			<div class="login-box">
				@if($error)
					<div class="alert alert-danger">
						@if($error == 'invalid_credentials')
							<p><strong>Invalid credentials</strong></p>
							<p>The credentials you entered are incorrect, please try again.</p>
						@endif
					</div>
				@endif
				<form action="/auth/login" method="post">
					@csrf
					<div class="form-group">
						<input type="text" name="email" class="form-control" placeholder="Email address" />
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Password" />
					</div>
					<input type="submit" class="btn btn-primary" value="Login" />
				</form>
			</div>
		</div>
	</div>
@endsection
@extends('generic.layout')

@section('content')
	<div class="page page-cart">
		<div class="hero">
			<div class="caption">
				<h2>Your Cart</h2>
				<h3>Sit amet, consectetur adipiscing elit</h3>
			</div>
		</div>

		<div class="cart">
			<div class="container">
				@if($cart->total_items > 0)
					<div class="cart-contents">
						@foreach($cart->items as $item)
							<div class="cart-item">
								<div class="image">
									<img src="{{ $item->product->image_thumbnail }}" class="img-fluid" />
								</div>
								<div class="info">
									<h2>{{ $item->quantity }} x {{ $item->product->name }} @if($item->cheese)<span class="badge">with cheese</span>@endif</h2>
									<p><strong>Fillings:</strong> @if($item->filling_list){{ $item->filling_list }}@else None @endif</p>
									<p><strong>Sauces:</strong> @if($item->sauce_list){{ $item->sauce_list }}@else None @endif</p>
								</div>
								<div class="price">
									<h3>&pound;{{ $item->price }}</h3>
									<a href="{{ route('cart_remove', ['item' => $item->id]) }}">Remove</a>
								</div>
							</div>
						@endforeach

						<div class="cart-total">
							<h2 class="right">Total cart cost <span>&pound;{{ $cart->total_price }}</span></h2>
						</div>
					</div>
					<div class="payment">
						@if(Auth::user())
							<payment-user></payment-user>
						@else
							<payment-guest></payment-guest>
						@endif
					</div>
				@else
					<div class="empty-cart">
						<span class="fa fa-shopping-basket"></span>
						<h2>Your cart is empty</h2>
						<p>You have no items in your cart, use the button below to browse through our selection of food and drinks</p>
						<a href="{{ route('categories') }}" class="btn btn-primary">Browse Menu</a>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection
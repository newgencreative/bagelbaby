@extends('generic.layout')

@section('content')
	<div class="page page-account">
		<div class="hero">
			<div class="caption">
				<h2>My Account</h2>
				<h3>Sit amet, consectetur adipiscing elit</h3>
			</div>
		</div>

		<div class="content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						@include('account.nav')
					</div>

					<div class="col-md-8">
						<a href="#" class="open-new-address">New address</a>

						<list-address></list-address>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
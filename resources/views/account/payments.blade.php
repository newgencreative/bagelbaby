@extends('generic.layout')

@section('content')
	<div class="page page-account">
		<div class="hero">
			<div class="caption">
				<h2 class="animated slideInDown">My Account</h2>
				<h3 class="animated slideInUp">Sit amet, consectetur adipiscing elit</h3>
			</div>
		</div>

		<div class="content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						@include('account.nav')
					</div>

					<div class="col-md-8">
						<a href="#" class="open-new-card">New card</a>

						<div class="list-group">
							@foreach($cards as $card)
								<div class="list-group-item">
									<div class="row">
										<div class="col-md-8">
											<p>**** **** **** {{ $card->card->last4 }}</p>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
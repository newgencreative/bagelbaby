<div class="list-group account-nav">
	<a href="{{ route('account_home') }}" class="list-group-item @if(Route::currentRouteName() == "account_home") active @endif">My Account</a>
	<a href="{{ route('account_orders') }}" class="list-group-item @if(Route::currentRouteName() == "account_orders") active @endif">Order History</a>
	<a href="{{ route('account_addresses') }}" class="list-group-item @if(Route::currentRouteName() == "account_addresses") active @endif">Addresses</a>
	<a href="{{ route('account_payments') }}" class="list-group-item @if(Route::currentRouteName() == "account_payments") active @endif">Payment Details</a>
</div>
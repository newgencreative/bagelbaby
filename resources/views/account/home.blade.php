@extends('generic.layout')

@section('content')
	<div class="page page-account">
		<div class="hero">
			<div class="caption">
				<h2>My Account</h2>
				<h3>Sit amet, consectetur adipiscing elit</h3>
			</div>
		</div>

		<div class="content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						@include('account.nav')
					</div>

					<div class="col-md-8">
						<form class="stylised">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Full name" value="{{ $user->name }}" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Full name" value="{{ $user->email }}" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input type="password" class="form-control" placeholder="Password" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<a href="#" class="btn btn-primary">Save changes</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
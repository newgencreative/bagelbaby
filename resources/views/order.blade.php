@extends('generic.newlayout')

@section('header')
	@include('generic.newheader')
@endsection

@section('content')
	<div class="page page-account page-account-order page-account-order-new">
		<div class="content">
			<div class="container">
				<div class="detail-wrapper">
					<div class="order-details">
						<h2>
							New Order
							<small>Placed {{ $order->human_time }}</small>
							<span class="float-right">Delivering at <span>{{ $order->pretty_delivery_time }}</span></span>
						</h2>
						@foreach($order->lines as $item)
							@if($item->product)
								<div class="order-item">
									<div class="image">
										<img src="{{ $item->product->image_thumbnail }}" class="img-fluid" />
									</div>
									<div class="info">
										<h2>{{ $item->product->name }} @if($item->cheese)<span class="badge">with cheese</span>@endif</h2>
										<p><strong>Fillings:</strong> @if($item->filling_list){{ $item->filling_list }}@else None @endif</p>
										<p><strong>Sauces:</strong> @if($item->sauce_list){{ $item->sauce_list }}@else None @endif</p>
									</div>
									<div class="price">
										<h3>&pound;{{ $item->product->price }}</h3>
									</div>
								</div>
							@elseif($item->deal)
								<div class="order-item deal-item">
									<div class="image">
										<img src="{{ $item->deal->image_thumbnail }}" class="img-fluid" />
									</div>
									<div class="info">
										<h2>{{ $item->deal->name }}</h2>
										@foreach($item->deal_options as $option)
											<p>{{ $option->product->name }}</p>
										@endforeach
									</div>
									<div class="price">
										<h3>&pound;{{ $item->deal->price }}</h3>
									</div>
								</div>
							@endif
						@endforeach

						<div class="order-total">
							<h2 class="right">Total order cost <span>&pound;{{ $order->total_price }}</span></h2>
						</div>
					</div>

					<div class="payment-details">
						<header>
							<h2>Order details</h2>
							<p>Below are your payment details and any customer information relating to your order</p>
						</header>
						<div class="body">
							<div class="jp-card-container"><div class="jp-card jp-card-visa jp-card-identified"><div class="jp-card-front"><div class="jp-card-logo jp-card-elo"><div class="e">e</div><div class="l">l</div><div class="o">o</div></div><div class="jp-card-logo jp-card-visa">Visa</div><div class="jp-card-logo jp-card-visaelectron">Visa<div class="elec">Electron</div></div><div class="jp-card-logo jp-card-mastercard">Mastercard</div><div class="jp-card-logo jp-card-maestro">Maestro</div><div class="jp-card-logo jp-card-amex"></div><div class="jp-card-logo jp-card-discover">discover</div><div class="jp-card-logo jp-card-dinersclub"></div><div class="jp-card-logo jp-card-dankort"><div class="dk"><div class="d"></div><div class="k"></div></div></div><div class="jp-card-logo jp-card-jcb"><div class="j">J</div><div class="c">C</div><div class="b">B</div></div><div class="jp-card-lower"><div class="jp-card-shiny"></div><div class="jp-card-cvc jp-card-display jp-card-valid">111</div><div class="jp-card-number jp-card-display jp-card-valid">**** **** **** {{ $payment->charges->data[0]->payment_method_details->card->last4 }}</div><div class="jp-card-name jp-card-display jp-card-valid">J Emery</div><div class="jp-card-expiry jp-card-display jp-card-valid" data-before="month/year" data-after="validthru">{{ $payment->charges->data[0]->payment_method_details->card->exp_month }}/{{ $payment->charges->data[0]->payment_method_details->card->exp_year }}</div></div></div><div class="jp-card-back"><div class="jp-card-bar"></div><div class="jp-card-cvc jp-card-display jp-card-valid">***</div><div class="jp-card-shiny"></div></div></div></div>
						</div>
						<footer>
							<div class="address">
								<div class="row">
									<div class="col-md-6">
										<h2>Address</h2>
										<p>{{ $order->address->line1 }}</p>
										<p>{{ $order->address->line2 }}</p>
										<p>{{ $order->address->town }}</p>
										<p>{{ $order->address->postcode }}</p>
									</div>
									<div class="col-md-6">
										<h2 class="text-right">Delivery</h2>
										@if($order->guest)
											<p class="text-right">{{ $order->guest->name }}</p>
										@else
											<p class="text-right">{{ $order->user->name }}</p>
										@endif
										<p class="text-right">{{ $order->pretty_date }}</p>
										<p class="text-right">4:30pm</p>
									</div>
								</div>
							</div>
						</footer>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
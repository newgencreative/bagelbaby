<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BagelBaby</title>
        <link href="/css/app.css?v=1" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app">
            @include('generic.header')
        	@yield('content')
            @include('generic.footer')
        </div>

        @yield('scripts')
        <script type="text/javascript">
            var _categories = {!! $categories !!};
            var _cart = {!! $cart !!};
            var _sauces = {!! $sauces !!};
            var _fillings = {!! $fillings !!};
        </script>
        <script src="https://js.stripe.com/v3/"></script>
        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
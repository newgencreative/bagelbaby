<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BagelBaby</title>
        <link href="/css/app.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app">
            @include('generic.header')
        	@yield('content')
            @include('generic.footer')

            <!-- Vue Components -->
            <popup-add-to-cart v-if="$root.activeAddProduct"></popup-add-to-cart>
            <popup-login></popup-login>
            <popup-register></popup-register>
            <popup-new-card></popup-new-card>
            <popup-new-address></popup-new-address>
        </div>

        <script type="text/javascript">
            window._sauces = {!! $sauces !!};
            window._fillings = {!! $fillings !!};
            window._cart_total_items = {{ $cart->total_items }};
            @if(isset($user))
                window._addresses = {!! $user->addresses !!};
            @endif
        </script>
        <script src="https://js.stripe.com/v3/"></script>
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.open-login').on('click', function(e) {
                    e.preventDefault();
                    $('.popup-login').fadeIn(300);
                });

                $('.open-register').on('click', function(e) {
                    e.preventDefault();
                    $('.popup-register').fadeIn(300);
                });

                $('.open-new-card').on('click', function(e) {
                    e.preventDefault();
                    $('.popup-new-card').fadeIn(300);
                });

                $('.open-new-address').on('click', function(e) {
                    e.preventDefault();
                    $('.popup-new-address').fadeIn(300);
                });
            });
        </script>
        @yield('scripts')
    </body>
</html>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BagelBaby</title>
        <link href="/css/app.css?v=1" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app">
            @yield('header')
        	@yield('content')
            @yield('footer')
        </div>

        @yield('scripts')
        <script type="text/javascript">
            var _categories = {!! $categories !!};
            var _cart = {!! $cart !!};
            var _sauces = {!! $sauces !!};
            var _fillings = {!! $fillings !!};
        </script>
        <script src="https://js.stripe.com/v3/"></script>
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.open-cart').on('click', function(e) {
                    e.preventDefault();

                    $('.shopping-basket').fadeOut(500);
                    $('.smart-cart').animate({
                        right: 0,
                    }, 500);

                    var width = $('body').innerWidth() - 350;

                    $('.menu-content, header.new').animate({
                        width: width,
                    }, 500);
                });

                $('.mobile-menu').on('click', function(e) {
                    e.preventDefault();

                    $('header.new .tabs').animate({
                        left: 0
                    }, 300);
                });

                $('.mobile-cart').on('click', function(e) {
                    e.preventDefault();

                    $('.smart-cart').animate({
                        right: 0,
                    }, 300).css('width', '100%');
                });

                $('.close-mobile-menu').on('click', function(e) {
                    e.preventDefault();

                    $('header.new .tabs').animate({
                        left: '-100%'
                    }, 300);
                });
            });
        </script>
    </body>
</html>
<header class="main @if(isset($fixed_header)) fixed @endif">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h1 class="logo"><a href="{{ route('home') }}"><img src="/images/logo-condensed.png" /><p>BagelBaby</p></a></h1>
			</div>
			<div class="col-md-9">
				<ul class="navigation">
					<li @if(Route::currentRouteName() == "newest_home")class="active"@endif><a href="{{ route('home') }}">Home</a></li>
					<li><a href="{{ route('menu') }}">Menu</a></li>
					<li><a href="{{ route('cart') }}">Cart <badge-cart></badge-cart></a></li>

					<button-close-mobile-menu></button-close-mobile-menu>
				</ul>
			</div>
		</div>

		<button-open-mobile-menu></button-open-mobile-menu>
	</div>
</header>
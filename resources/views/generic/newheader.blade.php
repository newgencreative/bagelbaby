<header class="new @if(isset($mini)) mini @endif @if(isset($nopad)) no-pad @endif">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1 class="logo"><a href="{{ route('new_home') }}"><img src="/images/logo-condensed.png" /><p>BagelBaby</p></a></h1>
            </div>
            <div class="col-md-8">
                <div class="tabs">
                    <ul>
                        <li @if(Route::currentRouteName() == "new_home")class="active"@endif><a href="/">Home</a></li>
                        <li @if(Route::currentRouteName() == "menu")class="active"@endif><a href="/menu">Menu</a></li>
                        <li class="shopping-basket"><a href="#" class="open-cart"><span class="fa fa-shopping-basket"></span><badge-cart></badge-cart></a></li>
                    </ul>

                    <a href="#" class="close-mobile-menu"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" class="mobile-menu"><span class="fa fa-bars"></span></a>
    <a href="#" class="mobile-cart"><span class="fa fa-shopping-basket"></span> <badge-cart></badge-cart></a>
</header>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-6 clearfix">
				<img src="/images/logo-condensed.png" class="logo" />
			</div>
			<div class="col-md-6 clearfix">
				<h2>BagelBaby</h2>
				<h3>Londons best bagels</h3>
			</div>
		</div>
	</div>
</footer>
<div class="post-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p>BagelBaby &copy; 2019 | All Rights Reserved</p>
			</div>
			<div class="col-md-6">
				<ul>
					<li><a href="#">Terms &amp; Conditions</a></li>
					<li><a href="#">Privacy Policy</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
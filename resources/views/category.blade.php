@extends('generic.layout')

@section('content')
	<div class="page page-category">
		<div
			class="hero"
			style="background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.4)), url('{{ $category->image }}') no-repeat center center; background-attachment: fixed; background-size: cover;"
		>
			<div class="caption">
				<h2>{{ $category->name }}</h2>
				<h3>Sit amet, consectetur adipiscing elit</h3>
			</div>
		</div>

		<div class="products">
			<div class="container">
				<div class="row">
					@foreach($category->products as $product)
						<div class="col-md-4">
							<div class="product">
								<a href="{{ route('product', ['product' => $product->slug]) }}"><img src="{{ $product->image }}" class="img-fluid" /></a>
								<div class="info">
									<h2><a href="{{ route('product', ['product' => $product->slug]) }}">{{ $product->name }}</a></h2>
									<p>{{ $product->description }}</p>
									<a href="{{ route('product', ['product' => $product->slug]) }}" class="btn btn-primary">View product</a>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
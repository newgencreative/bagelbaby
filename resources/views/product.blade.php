@extends('generic.layout')

@section('content')
	<div class="page page-product">
		<div
			class="hero"
			style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ $product->image }}') no-repeat center center; background-attachment: fixed; background-size: cover;"
		>
			<h2>{{ $product->name }}</h2>
			<h3>From <strong>&pound;{{ $product->price }}</strong></h3>
			<div class="image owl-carousel">
				<img src="{{ $product->image_thumbnail }}" />
				<img src="http://placehold.it/500/222" />
				<img src="http://placehold.it/500/aaa" />
			</div>
		</div>
		<div class="info">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6">
								<h2>{{ $product->tagline }}</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								{!! $product->long_description !!}
							</div>
						</div>
						<div class="row add-to-cart">
							<div class="col-md-4">
								<button-add-to-cart product_id="{{ $product->id }}"></button-add-to-cart>
							</div>
							<div class="col-md-8">
								<h2>Only &pound;{{ $product->price }}</h2>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row image-list">
							<div class="col-md-6"><img src="http://placehold.it/500/111" class="img-fluid" /></div>
							<div class="col-md-6"><img src="http://placehold.it/500/777" class="img-fluid" /></div>
							<div class="col-md-6"><img src="http://placehold.it/500/777" class="img-fluid" /></div>
							<div class="col-md-6"><img src="http://placehold.it/500/111" class="img-fluid" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('.owl-carousel').owlCarousel({
				items: 1,
				loop: true,
			});
		});
	</script>
@endsection
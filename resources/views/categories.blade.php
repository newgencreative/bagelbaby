@extends('generic.layout')

@section('content')
	<div class="page page-categories">
		<div class="tabs">
			<div class="container">
				<ul>
					@foreach($categories as $cat)
						<li @if($category->slug == $cat->slug) class="active"@endif><a href="{{ route('categories', $cat->slug) }}">{{ $cat->name }}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="products">
			<div class="container">
				<div class="row">
					@foreach($category->products as $product)
						<div class="col-md-6">
							<div class="product">
								<div class="image" style="background: url({{ $product->image }}) no-repeat center center; background-size: cover;"></div>
								<div class="info">
									<h2>{{ $product->name }} <small class="float-right">&pound;{{ $product->price }}</small></h2>
									<h3>{{ $product->tagline }}</h3>
									<p>{{ $product->description }}</p>
								</div>
								<div class="row actions">
									<div class="col-md-6">
										<button-add-to-cart product_id="{{ $product->id }}"></button-add-to-cart>
									</div>
									<div class="col-md-6">
										<p>Select your options when you add to cart</p>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
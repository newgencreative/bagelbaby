@extends('generic.layout')

@section('content')
	<div class="page page-home">
		<div class="hero">
			<div class="caption">
				<img src="/images/logo-loop.gif" class="logo animated fadeIn" />
				<!-- <div class="logo-animation-sprite" id="restart-animation"></div> -->
				<a href="{{ route('categories') }}" class="btn btn-default">Browse our menu</a>
			</div>
		</div>

		<div class="blocks">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="block block-one">
							<a href="{{ route('categories', 'deals') }}">
								<img src="/images/banners/mealdeal.jpg" class="img-fluid" />
							</a>
						</div>
						<div class="block block-one">
							<a href="{{ route('location') }}">
								<img src="/images/banners/delivery.jpg" class="img-fluid" />
							</a>
						</div>
					</div>
					<div class="col-md-8">
						<div class="block block-two">
							<img src="/images/banners/deliverycharge.jpg" class="img-fluid" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="featured-product">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<h2>Try our brand new</h2>
						<h3>Jerk Chicken</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae elit ac lacus facilisis accumsan. Proin et tellus auctor, dignissim libero ut, mattis enim. Donec mollis nulla imperdiet mauris lobortis posuere.</p>
						<a href="{{ route('product', ['product' => 'jerk-chicken']) }}" class="btn btn-default">Read more</a>
					</div>
					<div class="col-md-5">
						<img src="/images/products/bagel1.webp" />
					</div>
				</div>
			</div>
		</div>

		<div class="products">
			<div class="container">
				<div class="row">
					@foreach($categories as $category)
						<div class="col-md-3">
							<div class="product">
								<a href="{{ route('categories', $category->slug) }}"><img src="{{ $category->image }}" class="img-fluid" /></a>
								<div class="info">
									<h2><a href="{{ route('categories', $category->slug) }}">{{ $category->name }}</a></h2>
									<p>{{ Str::limit($category->description, 50) }}</p>
									<a href="{{ route('categories', $category->slug) }}" class="btn btn-primary">Browse</a>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>

		<div class="featured-product alt">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<img src="/images/products/bagel2.png" />
					</div>
					<div class="col-md-7">
						<h2>Everybody's Favourite</h2>
						<h3>Egg &amp; Bacon</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae elit ac lacus facilisis accumsan. Proin et tellus auctor, dignissim libero ut, mattis enim. Donec mollis nulla imperdiet mauris lobortis posuere.</p>
						<a href="{{ route('product', ['product' => 'egg-and-bacon']) }}" class="btn btn-default">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	{{-- <script type="text/javascript">
		var in_viewport = true,
			el = document.getElementById('restart-animation');;

		setTimeout(function() {
			el.style.animationPlayState = 'running';
		}, 2000);

		$(window).scroll(function() {
			if($('.logo-animation-sprite').isInViewport()) {
				if(!in_viewport) {
					restartAnimation();
				}

				in_viewport = true;
			} else {
				in_viewport = false;
			}
		});

		function restartAnimation()
		{
			el.style.animation = 'none';
			el.offsetHeight; /* trigger reflow to apply the change immediately */
			el.style.animation = null;
			el.style.animationPlayState = 'running';
		}

		$.fn.isInViewport = function()
		{
		    var elementTop = $(this).offset().top;
		    var elementBottom = elementTop + $(this).outerHeight();

		    var viewportTop = $(window).scrollTop();
		    var viewportBottom = viewportTop + $(window).height();

		    return elementBottom > viewportTop && elementTop < viewportBottom;
		};
	</script> --}}
@endsection
@extends('generic.layout')

@section('content')
	<div class="page page-generic">
		<div class="hero">
			<div class="caption">
				<h2>{{ $page->name }}</h2>
				<h3>{{ $page->subtitle }}</h3>
			</div>
		</div>

		<div class="page-content">
			<div class="container">
				<h2>The meal deal <small class="float-right">Hello, world</small></h2>
			</div>
		</div>
	</div>
@endsection
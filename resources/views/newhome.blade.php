@extends('generic.newlayout')

@section('header')
	@include('generic.newheader')
@endsection

@section('content')
	<div class="page page-new-home">
		<div class="hero">
			<div class="container">
				<img src="/images/logo-loop-fixed.gif" />
				<input-postcode></input-postcode>
			</div>
		</div>

		<div class="popular">
			<div class="container">
				<h2>Most Popular Products</h2>

				<div class="row">
					<div class="col-md-5 offset-md-1">
						<div class="new-product">
							<div class="top">
								<img src="/images/products/white.jpg" class="img-fluid" />
								<h2>Eggs &amp; Bacon</h2>
								<h3>&pound;10.00</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.</p>
							</div>
							<div class="bottom">
								<a href="#" style="margin: 0;">View product</a>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="new-product">
							<div class="top">
								<img src="/images/products/white.jpg" class="img-fluid" />
								<h2>Eggs &amp; Bacon</h2>
								<h3>&pound;10.00</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula a pulvinar egestas, sapien arcu fermentum nisl, in dictum leo ante a dui.</p>
							</div>
							<div class="bottom">
								<a href="#" style="margin: 0;">View product</a>
							</div>
						</div>
					</div>

					<div class="col-md-10 offset-md-1">
						<a href="/menu" class="btn btn-primary">View our entire menu</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<smart-cart minimized="true"></smart-cart>
@endsection

@section('footer')
	@include('generic.footer')
@endsection
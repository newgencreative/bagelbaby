<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="soon">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BagelBaby - coming soon!</title>
        <link href="/css/app.css?v=1" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="coming-soon">
            <img src="/images/products/bagel1.webp" />
            <h2>BagelBaby</h2>
            <h3>Coming soon</h3>

            <img src="/images/logo-loop-fixed.gif" />
        </div>
        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
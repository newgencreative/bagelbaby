@extends('generic.chromelesslayout')

@section('content')
	<div class="page page-home-redesign">
		<header>
			<div class="container">
				<div class="row">
					<div class="col-md-9 offset-md-3">
						<ul>
							<li class="active"><a href="#">Home</a></li>
							<li><a href="#">Menu</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>

		<div class="header-hero">
			<div class="container">
				<img src="/images/logo-condensed.png" class="logo" />
				
				<div class="row">
					<div class="col-md-6">
						<h2>Delicious<span>Bagels</span></h2>
						<h3>Delivered to your door</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum efficitur fermentum ligula, non mollis nibh fringilla ornare.</p>

						<a href="#">Browse the Menu</a>
					</div>
					<div class="col-md-6">
						<img src="/images/products/bagel1.webp" class="img-fluid" />
					</div>
				</div>
			</div>
		</div>

		<div class="menu">
			<!-- <div class="container"> -->
				<smart-menu home="true"></smart-menu>
			<!-- </div> -->
		</div>

		<!-- <div class="deals">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<img src="/images/banners/mealdeal.jpg" class="img-fluid" />
						<img src="/images/banners/delivery.jpg" class="img-fluid" style="margin-top: 10px;" />
					</div>
					<div class="col-md-8">
						<img src="/images/banners/deliverycharge.jpg" class="img-fluid" style="margin-left: 10px;" />
					</div>
				</div>
			</div>
		</div> -->

		<modal-product></modal-product>
		<modal-product-info></modal-product-info>
	</div>
@endsection
@extends('generic.newlayout')

@section('header')
	@include('generic.newheader', ['mini' => true, 'nopad' => true])
@endsection

@section('content')
	<div class="page page-menu">
        <div class="menu-content">
            <smart-menu @if($active) active="{{ $active }}"@endif></smart-menu>
        </div>
		<smart-cart></smart-cart>
	</div>

	<modal-product></modal-product>
	<modal-product-info></modal-product-info>
@endsection
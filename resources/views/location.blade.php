@extends('generic.layout')

@section('content')
	<div class="page page-location">
		<div class="hero">
			<div class="caption">
				<h2>The Location</h2>
				<h3>Sit amet, consectetur adipiscing elit</h3>

				<input-location-search></input-location-search>
			</div>
		</div>

		<div id="map"></div>
	</div>
@endsection

@section('scripts')
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7IfAc-GGtoSQH1imyUoRcAo8oRUxfZ0Q&callback=initMap"></script>
	<script type="text/javascript">
		function initMap()
		{
			var styledMapType = new google.maps.StyledMapType([
			    {
			        "featureType": "all",
			        "elementType": "labels.text.fill",
			        "stylers": [
			            {
			                "saturation": 36
			            },
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 40
			            }
			        ]
			    },
			    {
			        "featureType": "all",
			        "elementType": "labels.text.stroke",
			        "stylers": [
			            {
			                "visibility": "on"
			            },
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 16
			            }
			        ]
			    },
			    {
			        "featureType": "all",
			        "elementType": "labels.icon",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "administrative",
			        "elementType": "geometry.fill",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 20
			            }
			        ]
			    },
			    {
			        "featureType": "administrative",
			        "elementType": "geometry.stroke",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 17
			            },
			            {
			                "weight": 1.2
			            }
			        ]
			    },
			    {
			        "featureType": "landscape",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 20
			            }
			        ]
			    },
			    {
			        "featureType": "poi",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 21
			            }
			        ]
			    },
			    {
			        "featureType": "road.highway",
			        "elementType": "geometry.fill",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 17
			            }
			        ]
			    },
			    {
			        "featureType": "road.highway",
			        "elementType": "geometry.stroke",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 29
			            },
			            {
			                "weight": 0.2
			            }
			        ]
			    },
			    {
			        "featureType": "road.arterial",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 18
			            }
			        ]
			    },
			    {
			        "featureType": "road.local",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 16
			            }
			        ]
			    },
			    {
			        "featureType": "transit",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 19
			            }
			        ]
			    },
			    {
			        "featureType": "water",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 17
			            }
			        ]
			    }
			]);

			var uluru = {lat: 51.5286416, lng: -0.1015987};
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 12,
				center: uluru,
				disableDefaultUI: true,
			});

			map.mapTypes.set('styled_map', styledMapType);
			map.setMapTypeId('styled_map');

			var cityCircle = new google.maps.Circle({
		      	strokeColor: '#208bd3',
		      	strokeOpacity: 0.8,
		      	strokeWeight: 2,
		      	fillColor: '#208bd3',
		      	fillOpacity: 0.35,
		      	map: map,
		      	center: uluru,
		      	radius: (3.5 * 1609.34),
		    });
		}
	</script>
@endsection
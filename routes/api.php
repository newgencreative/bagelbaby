<?php

/* Cart */
Route::group(['prefix' => 'cart'], function() {
	Route::post('/', 'CartController@add');
	Route::put('/{cart_item}', 'CartController@edit');
	Route::post('/deal', 'CartController@addDeal');
	Route::post('/checkout/user', 'CartController@userCheckout');
	Route::post('/checkout/guest', 'CartController@guestCheckout');
	Route::get('/delivery', 'CartController@delivery');
	Route::delete('/{item}', 'CartController@apiRemove');
});

/* Account */
Route::group(['prefix' => 'account'], function() {
	Route::post('/card', 'AccountController@createCard');
	Route::get('/cards', 'AccountController@cards');
	Route::get('/address', 'AccountController@addressesAjax');
	Route::post('/address', 'AccountController@createAddress');
});
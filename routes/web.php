<?php

Route::get('/', function() {
	return view('soon');
});

// Route::get('/', 'PageController@newhome')->name('new_home');
// Route::get('/old', 'PageController@home')->name('home');
// Route::get('/new', 'PageController@newestHome')->name('newest_home');
// Route::get('/menu/{active?}', 'PageController@menu')->name('menu');
// Route::get('/location', 'PageController@location')->name('location');

// /* Deals */
// Route::group(['prefix' => 'deal'], function() {
// 	Route::get('/{deal}', 'PageController@deal');
// });

// /* Categories */
// Route::group(['prefix' => 'categories'], function() {
// 	Route::get('/{category?}', 'CategoryController@all')->name('categories');
// });

// /* Products */
// Route::group(['prefix' => 'products'], function() {
// 	Route::get('/{product}', 'ProductController@single')->name('product');
// });

// /* Cart */
// Route::group(['prefix' => 'cart'], function() {
// 	Route::get('/', 'CartController@my')->name('cart');
// 	Route::get('/remove/{item}', 'CartController@remove')->name('cart_remove');
// });

// /* Account */
// Route::group(['prefix' => 'account', 'middleware' => 'auth'], function() {
// 	Route::get('/', 'AccountController@home')->name('account_home');
// 	Route::get('/orders', 'AccountController@orders')->name('account_orders');
// 	Route::get('/addresses', 'AccountController@addresses')->name('account_addresses');
// 	Route::get('/payments', 'AccountController@payments')->name('account_payments');
// });

// /* Orders */
// Route::get('/orders/{order}', 'AccountController@order')->name('order');
// Route::get('/ordersnew/{order}', 'PageController@order');

// /* Auth */
// Route::group(['prefix' => 'auth'], function() {
// 	Route::post('/login', 'AuthController@login');
// 	Route::get('/logout', 'AuthController@logout')->name('logout');
// });

// /* Admin */
// Route::get('/login', 'AdminController@login')->name('admin_login');
// Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
// 	Route::get('/', 'AdminController@dashboard')->name('admin_dashboard');

// 	// Products
// 	Route::group(['prefix' => 'products'], function() {
// 		Route::get('/', 'AdminController@products')->name('admin_products');
// 		Route::get('/new', 'AdminController@newProduct')->name('admin_product_new');

// 		Route::get('/categories', 'AdminController@categories')->name('admin_product_categories');
// 		Route::get('/categories/new', 'AdminController@newCategory')->name('admin_product_categories_new');
// 		Route::get('/categories/{category}', 'AdminController@editCategory')->name('admin_product_categories_edit');

// 		Route::get('/sauces', 'AdminController@sauces')->name('admin_product_sauces');
// 		Route::get('/sauces/new', 'AdminController@newSauce')->name('admin_product_sauces_new');
// 		Route::get('/sauces/{sauce}', 'AdminController@editSauce')->name('admin_product_sauces_edit');
// 		Route::get('/fillings', 'AdminController@fillings')->name('admin_product_fillings');
// 		Route::get('/fillings/new', 'AdminController@newFilling')->name('admin_product_fillings_new');
// 		Route::get('/fillings/{filling}', 'AdminController@editFilling')->name('admin_product_fillings_edit');

// 		Route::get('/{product}', 'AdminController@editProduct')->name('admin_product');

// 		Route::post('/new', 'AdminController@doNew');
// 		Route::post('/categories/new', 'AdminController@doNewCategory');
// 		Route::post('/categories/{category}', 'AdminController@doEditCategory');
// 		Route::post('/sauces/new', 'AdminController@doNewSauce');
// 		Route::post('/sauces/{sauce}', 'AdminController@doEditSauce');
// 		Route::post('/fillings/new', 'AdminController@doNewFilling');
// 		Route::post('/fillings/{filling}', 'AdminController@doEditFilling');
// 		Route::post('/{product}', 'AdminController@doEdit');
// 	});

// 	// Orders
// 	Route::group(['prefix' => 'orders'], function() {
// 		Route::get('/', 'AdminController@orders')->name('admin_orders');
// 	});

// 	// Pages
// 	Route::group(['prefix' => 'pages'], function() {
// 		Route::get('/', 'AdminController@pages')->name('admin_pages');
// 	});

// 	// Users
// 	Route::group(['prefix' => 'users'], function() {
// 		Route::get('/', 'AdminController@users')->name('admin_users');
// 	});
// });

// /* Page catch all */
// Route::get('/{page}', 'PageController@generic')->name('generic');